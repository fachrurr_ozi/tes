import java.util.Scanner;

public class suit {
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        System.out.println("Pemain 1 pilih suit:");
        String pilih1 = input.nextLine();
        System.out.println("Pemain 2 pilih suit:");
        String pilih2 = input.nextLine();

        if (pilih1 == "gunting" && pilih2 == "kertas"){
            System.out.println("Pemain 1 yang Menang");
        }
        else if (pilih1 == "gunting" && pilih2 == "batu"){
            System.out.println("Pemain 2 yang Menang");
        }
        else if (pilih1 == "kertas" && pilih2 == "batu"){
            System.out.println("Pemain 1 yang Menang");
        }
        else if (pilih1 == "kertas" && pilih2 == "gunting"){
            System.out.println("Pemain 2 yang Menang");
        }
        else if (pilih1 == "batu" && pilih2 == "kertas"){
            System.out.println("Pemain 2 yang Menang");
        }
        else if (pilih1 == "batu" && pilih2 == "gunting"){
            System.out.println("Pemain 1 yang Menang");
        }
        else{
            System.out.println("Hasilnya seri");
        }
    }

}
